/* Dado um nº inteiro n, n > 1, imprimir sua decomposicao em fatores primos, indicando tambem
 a multiplicidade de cada fator
 Exemplo: 	8 = 2*2*2
			20 = 2*2*5
			1000 = 2³*5³ */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int numeros_primos(int num){
	int divisor = 0, cont = 1;
	
	while(cont <= num){
		if((num % cont) == 0)
			divisor++;		
		cont++;
	}
	if(cont == num+1 && divisor == 2)
		return 1;
	return 0;
}

void calcula_multiplicidade(int n){
	int fator_primo = 1, multiplicidade = 0;

	while (!numeros_primos(fator_primo)){
		fator_primo++;
	}
	while (n > 1){
		while(!(n % fator_primo)){
		 	multiplicidade += 1;
		 	n = n / fator_primo;
		}
		if (multiplicidade > 0){
			int fator = 1, aux = 0;

			while( aux < multiplicidade ){
				fator *= fator_primo;
				aux++;
			}
			printf("Fator %d tem multiplicidade = %d resultando em: %d\n",fator_primo,multiplicidade,fator);	
		}
		fator_primo += 1;
		while (!numeros_primos(fator_primo)){
			fator_primo += 1;
		} 
		multiplicidade = 0;
	}
}

void main(){
	int n;	
	clock_t tempo;
	tempo = clock();
	
	printf("Informe um número inteiro >1: ");
	scanf("%d",&n);
	if(n > 1){
		calcula_multiplicidade(n);
		printf("\nInforme um número inteiro >1: ");
		scanf("%d",&n);
	}
	if(n <= 1)
		printf("\n[FIM DA EXECUÇÂO]");

	printf("Tempo:%f\n",(clock() - tempo) / (double)CLOCKS_PER_SEC);
}